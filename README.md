# AIX Disk I/O Perfstat Test Code

Test code for [investigating](https://github.com/oshi/oshi/issues/2239) *wierd* constant read stats on AIX.

Example from PowerVS AIX 7.3 

```
# ./aix-disk-perfstat 2
Sat Nov 12 14:29:28 2022 - hdisk0 => reads: [ blocks:   41710376 (       0 ), bytes: 4175843328 (       0 ) ], writes: [ blocks:   52579577 (       0 ), bytes: 1150939648 (       0 ) ]
Sat Nov 12 14:29:30 2022 - hdisk0 => reads: [ blocks:   41710376 (       0 ), bytes: 4175843328 (       0 ) ], writes: [ blocks:   52579577 (       0 ), bytes: 1150939648 (       0 ) ]
Sat Nov 12 14:29:32 2022 - hdisk0 => reads: [ blocks:   41710376 (       0 ), bytes: 4175843328 (       0 ) ], writes: [ blocks:   52580401 (     824 ), bytes: 1151361536 (  421888 ) ]
Sat Nov 12 14:29:34 2022 - hdisk0 => reads: [ blocks:   41714586 (    4210 ), bytes: 4177998848 ( 2155520 ) ], writes: [ blocks:   52580473 (      72 ), bytes: 1151398400 (   36864 ) ]
Sat Nov 12 14:29:36 2022 - hdisk0 => reads: [ blocks:   41714586 (       0 ), bytes: 4177998848 (       0 ) ], writes: [ blocks:   52580473 (       0 ), bytes: 1151398400 (       0 ) ]
Sat Nov 12 14:29:38 2022 - hdisk0 => reads: [ blocks:   41714586 (       0 ), bytes: 4177998848 (       0 ) ], writes: [ blocks:   52580473 (       0 ), bytes: 1151398400 (       0 ) ]
Sat Nov 12 14:29:40 2022 - hdisk0 => reads: [ blocks:   41714586 (       0 ), bytes: 4177998848 (       0 ) ], writes: [ blocks:   52580473 (       0 ), bytes: 1151398400 (       0 ) ]
Sat Nov 12 14:29:42 2022 - hdisk0 => reads: [ blocks:   41714586 (       0 ), bytes: 4177998848 (       0 ) ], writes: [ blocks:   52580473 (       0 ), bytes: 1151398400 (       0 ) ]
Sat Nov 12 14:29:44 2022 - hdisk0 => reads: [ blocks:   41718796 (    4210 ), bytes: 4180154368 ( 2155520 ) ], writes: [ blocks:   52580553 (      80 ), bytes: 1151439360 (   40960 ) ]
Sat Nov 12 14:29:46 2022 - hdisk0 => reads: [ blocks:   41718796 (       0 ), bytes: 4180154368 (       0 ) ], writes: [ blocks:   52580553 (       0 ), bytes: 1151439360 (       0 ) ]
Sat Nov 12 14:29:48 2022 - hdisk0 => reads: [ blocks:   41718796 (       0 ), bytes: 4180154368 (       0 ) ], writes: [ blocks:   52580553 (       0 ), bytes: 1151439360 (       0 ) ]
Sat Nov 12 14:29:50 2022 - hdisk0 => reads: [ blocks:   41718796 (       0 ), bytes: 4180154368 (       0 ) ], writes: [ blocks:   52580553 (       0 ), bytes: 1151439360 (       0 ) ]
Sat Nov 12 14:29:52 2022 - hdisk0 => reads: [ blocks:   41718796 (       0 ), bytes: 4180154368 (       0 ) ], writes: [ blocks:   52580553 (       0 ), bytes: 1151439360 (       0 ) ]
Sat Nov 12 14:29:54 2022 - hdisk0 => reads: [ blocks:   41723006 (    4210 ), bytes: 4182309888 ( 2155520 ) ], writes: [ blocks:   52580625 (      72 ), bytes: 1151476224 (   36864 ) ]
Sat Nov 12 14:29:56 2022 - hdisk0 => reads: [ blocks:   41723006 (       0 ), bytes: 4182309888 (       0 ) ], writes: [ blocks:   52580625 (       0 ), bytes: 1151476224 (       0 ) ]
Sat Nov 12 14:29:58 2022 - hdisk0 => reads: [ blocks:   41723006 (       0 ), bytes: 4182309888 (       0 ) ], writes: [ blocks:   52580625 (       0 ), bytes: 1151476224 (       0 ) ]
Sat Nov 12 14:30:00 2022 - hdisk0 => reads: [ blocks:   41723006 (       0 ), bytes: 4182309888 (       0 ) ], writes: [ blocks:   52580625 (       0 ), bytes: 1151476224 (       0 ) ]
Sat Nov 12 14:30:02 2022 - hdisk0 => reads: [ blocks:   41723006 (       0 ), bytes: 4182309888 (       0 ) ], writes: [ blocks:   52580657 (      32 ), bytes: 1151492608 (   16384 ) ]
Sat Nov 12 14:30:04 2022 - hdisk0 => reads: [ blocks:   41727216 (    4210 ), bytes: 4184465408 ( 2155520 ) ], writes: [ blocks:   52580737 (      80 ), bytes: 1151533568 (   40960 ) ]
Sat Nov 12 14:30:06 2022 - hdisk0 => reads: [ blocks:   41727216 (       0 ), bytes: 4184465408 (       0 ) ], writes: [ blocks:   52580737 (       0 ), bytes: 1151533568 (       0 ) ]
Sat Nov 12 14:30:08 2022 - hdisk0 => reads: [ blocks:   41727216 (       0 ), bytes: 4184465408 (       0 ) ], writes: [ blocks:   52580737 (       0 ), bytes: 1151533568 (       0 ) ]
Sat Nov 12 14:30:10 2022 - hdisk0 => reads: [ blocks:   41727216 (       0 ), bytes: 4184465408 (       0 ) ], writes: [ blocks:   52580737 (       0 ), bytes: 1151533568 (       0 ) ]
Sat Nov 12 14:30:12 2022 - hdisk0 => reads: [ blocks:   41727216 (       0 ), bytes: 4184465408 (       0 ) ], writes: [ blocks:   52580737 (       0 ), bytes: 1151533568 (       0 ) ]
Sat Nov 12 14:30:14 2022 - hdisk0 => reads: [ blocks:   41731426 (    4210 ), bytes: 4186620928 ( 2155520 ) ], writes: [ blocks:   52580849 (     112 ), bytes: 1151590912 (   57344 ) ]
```
