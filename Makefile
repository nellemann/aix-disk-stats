#
# Use GNU-Make (gmake) for building
#

bin = aix-disk-perfstat
src = $(wildcard *.c)
obj = $(src:.c=.o)

CC = gcc
LDFLAGS = -lperfstat

$(bin): $(obj)
	$(CC) -o $@ $^ $(LDFLAGS)

.PHONY: clean
clean:
	rm -f $(obj) $(bin)
